package com.squgeim.xhifttorrent;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by squgeim on 8/19/16.
 */
public class RequestPieces {

    private static RequestPieces instance = new RequestPieces();

    final int PIECE_LENGTH = (int) MetainfoDict.getInstance().getPieceLength();
    final int BLOCK_SIZE = 16 * 1024; // Hard coding to 16KB for now!!
    final int BLOCK_COUNT = (int) Math.ceil(PIECE_LENGTH / BLOCK_SIZE);

    private HashMap<Integer, Piece> currentlyOpenPieces = new HashMap<>();

    // this list contains all the blocks that have been requested. The values will be removed as soon as the block
    // is available.

    // The integer is the absolute index of the block in the file.
    // ie, piece 4, block 2 is (4 * no_of_block_in_each_piece) + 2

    private LinkedList<Long> currentRequestQueue = new LinkedList<>();

    private RequestPieces() {}

    public static RequestPieces getInstance() {
        return instance;
    }

    public Piece getPiece(int index) throws PieceNotOpen {
        if(currentlyOpenPieces.get(index) != null) {
            return currentlyOpenPieces.get(index);
        } else {
            throw new PieceNotOpen();
        }
    }

    public Piece openPiece(int index) {
        if(currentlyOpenPieces.get(index) != null) {
            return currentlyOpenPieces.get(index);
        } else {
            Piece new_piece = new Piece(index);
            currentlyOpenPieces.put(index, new_piece);
            return new_piece;
        }
    }

    public boolean isPieceOpen(int index) {
        return currentlyOpenPieces.containsKey(index);
    }

    public int getCurrentPiecesNumber() {
        return currentlyOpenPieces.size();
    }

    public int getCurrentRequestsNumber() {
        return currentRequestQueue.size();
    }

    public void newRequestBlock(int piece_index, int block_index, Peers.Peer peer) {
        currentRequestQueue.add((long) (piece_index * BLOCK_COUNT + block_index));
    }

    private long getAbsoluteIndex(int piece_index, int block_index) {
        return piece_index * BLOCK_COUNT + block_index;
    }

    private class PieceNotOpen extends Exception {}

    public class Piece {

        int index;

        BitSet bitfield = new BitSet(BLOCK_COUNT);

        LinkedList<Integer> requested_blocks = new LinkedList<>();

        ByteBuffer piece = ByteBuffer.allocate(PIECE_LENGTH);

        Piece(int index) {
            this.index = index;
        }

        public void addBlock(int block_index, byte[] content) throws IOException {
            if(! requested_blocks.contains(block_index)) {
                // Not a requested block.
                // Silently ignore.
                return;
            }

            piece.put(content, block_index, BLOCK_SIZE);
            bitfield.set(block_index);

            requested_blocks.remove(block_index);

            long absolute_index = index * BLOCK_COUNT + block_index;
            currentRequestQueue.remove(absolute_index);

            if(isFull()) {
                writeToFile();
            }
        }

        public boolean isRequested(int block_index) {
            return requested_blocks.contains(block_index);
        }

        public void addRequestedBlock(int block_index) {
            requested_blocks.add(block_index);
        }

        public BitSet getBitfield() {
            return bitfield;
        }

        public ArrayList<Integer> getEmptyBlocks() {
            ArrayList<Integer> l = new ArrayList<>();

            int index = 0;
            while(true) {
                index = bitfield.nextClearBit(index);

                if(index == -1) {
                    break;
                } else {
                    l.add(index);
                }
            }

            return l;
        }

        private boolean isFull() {
            if(bitfield.nextClearBit(0) == -1) {
                // bitfield is full.
                return true;
            } else {
                return false;
            }
        }

        private void writeToFile() throws IOException {
            DownloadFile file = DownloadFile.getInstance();
            file.write(index, piece);
        }

    }
}
