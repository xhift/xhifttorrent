/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squgeim.xhifttorrent;

import com.squgeim.xhiftbencoder.BencodeDataType;
import com.squgeim.xhiftbencoder.BencodeDictionary;
import com.squgeim.xhiftbencoder.BencodeList;
import com.squgeim.xhiftbencoder.BencodeString;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.*;

/**
 *
 * @author squgeim
 */
public class XhiftTorrent {
    
    private final MetainfoDict metainfo;

    private boolean tracker = false;

    private Timer timer = new Timer("Announcer",true);
    
    private final int PORT = 6969;

    private final int TOTAL_REQUEST_LIMIT = 10;

    private String downloadPath;

    private byte[] peerId;

    private static XhiftTorrent instance = null;

    public static XhiftTorrent init(String torrentFilePath, String downloadPath) throws IOException {
        if(instance == null) {
            instance = new XhiftTorrent(torrentFilePath);
            instance.downloadPath = downloadPath;
        }
        return instance;
    }

    public static XhiftTorrent init(BencodeDictionary torrentDict, String downloadPath) throws IOException {
        if(instance == null) {
            instance = new XhiftTorrent(torrentDict);
            instance.downloadPath = downloadPath;
        }
        return instance;
    }

    public static XhiftTorrent getInstance() {
        return instance;
    }

    private XhiftTorrent(String torrentFilePath) throws IOException {
        File torrentFile = new File(torrentFilePath);
        metainfo = MetainfoDict.init(torrentFile);
        setPeerId();
    }
    
    private XhiftTorrent(BencodeDictionary torrentDict) throws IOException {
        metainfo = MetainfoDict.init(torrentDict);
        setPeerId();
    }

    private void setPeerId() throws IOException {
        ByteArrayOutputStream peerId_bs = new ByteArrayOutputStream(20);
        peerId_bs.write("-XT0001-".getBytes(StandardCharsets.UTF_8));

        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder( 12 );
        for( int i = 0; i < 12; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );

        peerId_bs.write(sb.toString().getBytes(StandardCharsets.UTF_8));

        peerId = peerId_bs.toByteArray();
    }

    private void announcePeer() {
        ByteBuf msg = Unpooled.buffer();
        msg.writeBytes("GET /a?".getBytes(StandardCharsets.UTF_8));

        BencodeDictionary announce_request = new BencodeDictionary();
        announce_request.put("peer_id", peerId);
        announce_request.put("info_hash", MetainfoDict.getInstance().getInfoHash());
        announce_request.put("PORT", PORT);

        try {
            msg.writeBytes(URLEncoder.encode(announce_request.getBencode(),"UTF-8").getBytes(StandardCharsets.UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        msg.writeBytes(" HTTP/1.1".getBytes(StandardCharsets.UTF_8));

        URL announce_url = metainfo.getTracker();
        new XhiftClient(announce_url.getHost(), announce_url.getPort(), msg);
    }

    public void setInterval(int interval) { // interval is in seconds
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("here");
                announcePeer();
            }
        }, interval * 1000);
    }

    public void insertPeers(BencodeList peers) {
        ArrayList<BencodeString> peerIds = new ArrayList<>();
        for(BencodeDataType peer : peers) {
            Peers.getInstance().addPeer((BencodeDictionary) peer);
            peerIds.add((BencodeString)((BencodeDictionary)peer).get("peer id"));
        }
        HashMap<BencodeString, Peers.Peer> allPeers = Peers.getInstance().getAllPeers();
        for(Iterator<Map.Entry<BencodeString, Peers.Peer>> it = allPeers.entrySet().iterator(); it.hasNext();) {
            Map.Entry<BencodeString, Peers.Peer> entry = it.next();
            if(! peerIds.contains(entry.getKey())) {
                it.remove();
            }
        }
    }

    public BencodeString getPeerId() {
        return new BencodeString(peerId);
    }

    public boolean setTrackerState(boolean state) {
        tracker = state;
        return tracker;
    }

    private boolean getUpdatedInterestLevel(Peers.Peer peer) {
        BitSet my_bitfield = (BitSet) Peers.getInstance().getMe().getBitfield().clone();
        BitSet peer_bitfield = (BitSet) peer.getBitfield().clone();

        // if A = peer and B = me, then (A & !B) gives each piece unique to A
        my_bitfield.flip(0, my_bitfield.length());
        peer_bitfield.and(my_bitfield);

        if(peer_bitfield.cardinality() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void run() {
        // ok, so i need to be more carefull with the interested messages. saying im interesed and then when i actually
        // get unchoked if don't request for the piece, then the peer will probably get stuck. so i'll only send an
        // interested message when i'm sure i'm going to request if unchoked. but this means, i'll need to keep a note
        // of what piece i'm interested in when i send a interested message, because trying to find out what piece to
        // ask when i get unchoked is another complexity.

        // so what i'll do it, maintain a list of interested pieces for each peer. then just send send an interested
        // message when there is something in this list. and when unchoked, just send a request for a piece in this
        // list. two birds, one list. :D

        // sounds cool to me. let's see.

        // Step 1. See what piece I need.
        // Step 2. Decide who to get each piece from.
        // Step 3. Update the interested pieces list for each of those peers.
        // Step 4. If the requested_pieces list have n slots, then choose n peers at random (?) and send an interested
        //         message.

        // fuck all that. new strategy.

        // keep a list of current requests.
        // if the total requests are less than the total limit, then get n of the required pieces.

        announcePeer();

        Peers.Peer me = Peers.getInstance().getMe();

        while(true) {

            int current_open_pieces = RequestPieces.getInstance().getCurrentRequestsNumber();

            if(current_open_pieces < TOTAL_REQUEST_LIMIT) {
                int blocks_needed = TOTAL_REQUEST_LIMIT - current_open_pieces;

                ArrayList<Integer> required_pieces_indices = me.getNeededPieces();


                LinkedList<Integer> interested_blocks = new LinkedList<>();

                for(int i = 0; i < blocks_needed; ) {
                    int piece_index = required_pieces_indices.get(new Random().nextInt(required_pieces_indices.size()));
                    RequestPieces.Piece p = RequestPieces.getInstance().openPiece(piece_index);

                    ArrayList<Integer> needed_blocks = p.getEmptyBlocks();

                    if(needed_blocks.size() < (blocks_needed - i)) {
                        interested_blocks.addAll(needed_blocks);
                        i += needed_blocks.size();
                    } else {
                        interested_blocks.addAll(needed_blocks.subList(0, (blocks_needed - i)));
                        i += blocks_needed;
                    }
                }

                // got the interested blocks.
                // now need to divide this among the peers.



            } else {
                // no new piece request needed.
                System.out.println("No new piece request needed.");
            }

        }

    }

    public void start() throws IOException {
        if(tracker == true) {
            XhiftTracker.init();
        }

        DownloadFile.init(downloadPath);

//        byte[] file_bitfield = DownloadFile.getInstance().getFileStatus();
//        Peers.getInstance().getMe().loadBitfield(file_bitfield);

        Thread server_thread = new Thread(new XhiftServer(PORT));
        server_thread.start();

//        Thread client_thread = new Thread(instance);
//        client_thread.start();

        this.run();
    }
    
    public static void main(String[] argv) throws IOException {
        XhiftTorrent client = XhiftTorrent.init("/home/squgeim/pic.torrent", "/home/squgeim/pic.jpg");
        client.setTrackerState(true);
        client.start();
    }
    
}
