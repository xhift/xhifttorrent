package com.squgeim.xhifttorrent;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.BitSet;

/**
 * Created by squgeim on 8/15/16.
 */
public class DownloadFile {

    static private DownloadFile instance;

    private RandomAccessFile file;
    private FileChannel file_channel;

    public static void init(String downloadPath) throws IOException {
        if(instance == null) {
            instance = new DownloadFile(downloadPath);
        }
    }

    static public DownloadFile getInstance() {
        return instance;
    }

    private DownloadFile(String downloadPath) throws IOException {
//        file_channel = AsynchronousFileChannel.open(Paths.get(downloadPath),
//                StandardOpenOption.WRITE, StandardOpenOption.CREATE);
        file = new RandomAccessFile(downloadPath, "rw");
        file_channel = file.getChannel();
    }

    public byte[] getFileStatus() throws IOException {
        int piece_length = (int) MetainfoDict.getInstance().getPieceLength();
        int num_pieces = MetainfoDict.getInstance().pieces();

        BitSet bitfield = new BitSet(num_pieces);

        ByteBuffer piece_bytes;

        for(int i = 0; i < num_pieces; i++) {
            piece_bytes = ByteBuffer.allocate(piece_length);

            file_channel.read(piece_bytes, piece_length * i);

            byte[] thisHash = getSha1Hash(piece_bytes);
            byte[] torrentHash = MetainfoDict.getInstance().getPieceHash(i);

            if(Arrays.equals(thisHash, torrentHash)) {
                // this piece is alright
                bitfield.set(i);
            }
       }

       return bitfield.toByteArray();
    }

    private byte[] getSha1Hash(ByteBuffer piece) {
        MessageDigest sha1digest = null;
        try {
            sha1digest = MessageDigest.getInstance("SHA1");
            sha1digest.update(piece);
            return sha1digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new byte[]{0x00};
    }

    public void write(int piece_index, ByteBuffer content) throws IOException {
        int position = (int) (piece_index * MetainfoDict.getInstance().getPieceLength());
        file_channel.write(content, position);
    }

}
