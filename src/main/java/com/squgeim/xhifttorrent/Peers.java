package com.squgeim.xhifttorrent;

import com.squgeim.xhiftbencoder.BencodeDictionary;
import com.squgeim.xhiftbencoder.BencodeInt;
import com.squgeim.xhiftbencoder.BencodeString;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by squgeim on 7/29/16.
 */
public class Peers {
    private static Peers instance = new Peers();

    private Peer me;

    private HashMap<BencodeString, Peer> peers = new HashMap<>();
    private HashMap<String, BencodeString> peer_ids = new HashMap<>();

    public ArrayList<Peer> is_interested_list = new ArrayList<>();
    public ArrayList<Peer> am_interested_list = new ArrayList<>();
    public ArrayList<Peer> is_not_choking = new ArrayList<>();
    public ArrayList<Peer> am_not_choking = new ArrayList<>();

    public static Peers getInstance() {
        return instance;
    }

    private Peers() {
        me = new Peer(XhiftTorrent.getInstance().getPeerId(), "127.0.0.1", 6969);
    }

    public void addPeer(BencodeDictionary peer_info) {
        Peer new_peer = new Peer((BencodeString) peer_info.get("peer id"),
                ((BencodeString) peer_info.get("ip")).getValue(), (int) ((BencodeInt) peer_info.get("port")).getValue());

        if (!peers.keySet().contains(new_peer.peerId)) {
            peers.put(new_peer.peerId, new_peer);
            peer_ids.put(new_peer.getPeerAddress(), new_peer.getPeerId());
        }
    }

    public Peer getPeer(BencodeString peerId) {
        return peers.get(peerId);
    }

    public HashMap<BencodeString, Peer> getAllPeers() {
        return peers;
    }

    public Peer getPeerByIp(String server_address) {
        return peers.get(peer_ids.get(server_address));
    }

    public Peer getMe() {
        return me;
    }

    public class Peer {

        private BencodeString peerId;
        private String host;
        private int port;

        private boolean handshake_status = false;

        private Date connection_time;

        private BitSet bitfield = new BitSet(MetainfoDict.getInstance().pieces());

        private boolean am_choking = true; // I am choking this peer
        private boolean am_interested = false; // I am interested in this peer
        private boolean peer_choking = true; // Peer is choking me
        private boolean peer_interested = false; // Peer is interested in me

        public BitSet getBitfield() {
            return bitfield;
        }

        public boolean getIsPeerChoking() {
            return peer_choking;
        }

        public boolean getIsPeerInterested() {
            return peer_interested;
        }

        public boolean getAmInterested() {
            return am_interested;
        }

        public boolean getAmChoking() {
            return am_choking;
        }

        private XhiftClient channel;

        Peer(BencodeString peerId, String host, int port) {
            this.peerId = peerId;
            this.host = host;
            this.port = port;
            bitfield.clear();
        }

        public void havePiece(int piece_index) {
            bitfield.set(piece_index);
        }

        public void loadBitfield(byte[] pieces) {
            int count = 0;
            for(int i = 0; i < pieces.length; i++) {
                for(int j = 0; j < 8; j++, count++) {
                    if(count == bitfield.size()) {
                       break;
                    }
                    if((pieces[i] & (128 >> j)) != 0) {
                        bitfield.set(count);
                    } else {
                        bitfield.clear(count);
                    }
                }
            }
        }

        public void handshake() {
            ByteBuf handshake_msg = Unpooled.buffer();
            byte[] protocol = "XhiftTorrent".getBytes(StandardCharsets.UTF_8);
            handshake_msg.writeByte(protocol.length);
            handshake_msg.writeBytes(protocol);
            handshake_msg.writeBytes(new byte[8]);
            handshake_msg.writeBytes(MetainfoDict.getInstance().getInfoHashBytes());
            handshake_msg.writeBytes(XhiftTorrent.getInstance().getPeerId().getValueBytes());

            new XhiftClient(host, port, handshake_msg);
        }

        public void keepAlive() {
            connection_time = new Date();
        }

        public void isChoking() {
            peer_choking = true;
            if(Peers.getInstance().is_not_choking.contains(this)) {
                Peers.getInstance().is_not_choking.remove(this);
            }
        }

        public void isUnchoking() {
            peer_choking = false;
            if(!Peers.getInstance().is_not_choking.contains(this)) {
                Peers.getInstance().is_not_choking.add(this);
            }
        }

        public void isInterested() {
            peer_interested = true;
            if(!Peers.getInstance().is_interested_list.contains(this)) {
                Peers.getInstance().is_interested_list.add(this);
            }
        }

        public void isNotInterested() {
            peer_interested = false;
            if(Peers.getInstance().is_interested_list.contains(this)) {
                Peers.getInstance().is_interested_list.remove(this);
            }
        }

        public void choke() {
            ByteBuf msg = Unpooled.buffer();
            msg.writeBytes(new byte[] { (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01 });
            msg.writeByte((byte) 0x00);
            new XhiftClient(host, port, msg);
            am_choking = true;

            if(Peers.getInstance().am_not_choking.contains(this)) {
                Peers.getInstance().am_not_choking.remove(this);
            }
        }

        public void unchoke() {
            ByteBuf msg = Unpooled.buffer();
            msg.writeBytes(new byte[] { (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01 });
            msg.writeByte((byte) 0x01);
            new XhiftClient(host, port, msg);
            am_choking = false;

            if(!Peers.getInstance().am_not_choking.contains(this)) {
                Peers.getInstance().am_not_choking.add(this);
            }
        }

        public void amInterested() {
            ByteBuf msg = Unpooled.buffer();
            msg.writeBytes(new byte[] { (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01 });
            msg.writeByte((byte) 0x02);
            new XhiftClient(host, port, msg);
            am_interested = true;

            if(! Peers.getInstance().am_interested_list.contains(this)) {
                Peers.getInstance().am_interested_list.add(this);
            }
        }

        public void amNotInterested() {
            ByteBuf msg = Unpooled.buffer();
            msg.writeBytes(new byte[] { (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01 });
            msg.writeByte((byte) 0x03);
            new XhiftClient(host, port, msg);
            am_interested = false;

            if(Peers.getInstance().am_interested_list.contains(this)) {
                Peers.getInstance().am_interested_list.remove(this);
            }
        }

        public ArrayList<Integer> getNeededPieces() {
            ArrayList<Integer> l = new ArrayList<>();

            int index = 0;
            while(true) {
                index = bitfield.nextClearBit(index);

                if (index == -1) {
                    break;
                } else {
                    l.add(index);
                }
            }

            return l;
        }

        public BencodeString getPeerId() {
            return peerId;
        }

        public String getPeerAddress() {
            return host;
        }

        public boolean isHandshaked() {
            return handshake_status;
        }

        public void hasHandshaked() {
            handshake_status = true;
        }

    }
}
