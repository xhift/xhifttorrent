package com.squgeim.xhifttorrent;

import com.squgeim.xhiftbencoder.*;
import com.squgeim.xhiftbencoder.throwables.IsNotBencode;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class ClientHandler extends ChannelInboundHandlerAdapter {
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws InvalidRequest {
		ByteBuf in = (ByteBuf) msg;
		byte first_byte = in.readByte();

        System.out.println("in client handler");

        String server_address = ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();

        if(server_address.equals(MetainfoDict.getInstance().getTracker().getHost())) {
            if(first_byte == 'd') {
                // Message may contain a bencoded dictionary (announce response).
                in.resetReaderIndex();
                byte[] response = new byte[in.readableBytes()];
                in.readBytes(response);
                try {
                    BencodeDictionary announce_response = (BencodeDictionary) BencodeDecoder.decodeBencode(response);

                    if (announce_response.get("failure reason") != null) {
                        System.out.println("There was a problem with the announce request.");
                        System.out.println(((BencodeString) announce_response.get("failure reason")).getValue());
                        return;
                    }

                    int interval = (int) ((BencodeInt) announce_response.get("interval")).getValue();
                    BencodeList peers = (BencodeList) announce_response.get("peers");

                    XhiftTorrent xhift = XhiftTorrent.getInstance();

                    xhift.setInterval(interval);
                    xhift.insertPeers(peers);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IsNotBencode isNotBencode) {
                    isNotBencode.printStackTrace();
                }
            }
            ctx.close();
            return;
        }
	}


	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

	public class InvalidRequest extends Exception {}
}
