/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squgeim.xhifttorrent;

import com.squgeim.xhiftbencoder.BencodeDecoder;
import com.squgeim.xhiftbencoder.BencodeDictionary;
import com.squgeim.xhiftbencoder.BencodeInt;
import com.squgeim.xhiftbencoder.BencodeString;
import com.squgeim.xhiftbencoder.throwables.IsNotBencode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author squgeim
 */
public class MetainfoDict {

    private static MetainfoDict instance;

    private BencodeDictionary torrentDict;
    private BencodeDictionary info;
    private String announce;
    private URL announce_url;
    private String name;
    private long length;
    private long piece_length;
    private byte[][] pieces;
    private String infoHash;
    private byte[] infoHashBytes;
    private static byte[] metainfoBytes;

    public static MetainfoDict init(BencodeDictionary torrentDict) throws MalformedURLException {
        if (instance == null) {
            instance = new MetainfoDict(torrentDict);
        }
        return instance;
    }

    public static MetainfoDict init(File torrentFile) throws FileNotFoundException {
        if (instance != null) {
            return instance;
        }

        FileInputStream inputStream = new FileInputStream(torrentFile);
        byte[] torrentFileBytes = new byte[(int) torrentFile.length()];
        try {
            inputStream.read(torrentFileBytes);
            inputStream.close();

            BencodeDictionary torrentDict = (BencodeDictionary) BencodeDecoder.decodeBencode(torrentFileBytes);

            instance = new MetainfoDict(torrentDict);
            metainfoBytes = torrentFileBytes;
        } catch (IOException | IsNotBencode ex) {
            Logger.getLogger(MetainfoDict.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instance;
    }

    public static MetainfoDict getInstance() {
        return instance;
    }

    private MetainfoDict(BencodeDictionary torrentDict) throws MalformedURLException {
        this.torrentDict = torrentDict;
        info = (BencodeDictionary) torrentDict.get("info");
        announce = ((BencodeString) torrentDict.get("announce")).getValue();
        announce_url = new URL(((BencodeString) torrentDict.get("announce")).getValue());
        name = (String) ((BencodeString) info.get("name")).getValue();
        length = ((BencodeInt) info.get("length")).getValue();
        piece_length = ((BencodeInt) info.get("piece length")).getValue();

        int piece_count = (int) Math.ceil((double) length / piece_length);
        pieces = getPieceHashes(((BencodeString) info.get("pieces")).getValueBytes(), piece_count);
        infoHash = setInfoHash();
        infoHashBytes = setInfoHashBytes();
        metainfoBytes = torrentDict.getBencodeBytes();
    }

    public byte[] getMetainfoBytes() {
        return metainfoBytes;
    }

    private byte[][] getPieceHashes(byte[] pieces, int pieceCount) {
        byte[][] pieces_list = new byte[pieceCount][20];
        int count = 0;
        for (int i = 0; i < pieceCount; i++) {
            for (int j = 0; j < 20; j++) {
                if (count == pieces.length) break;
                pieces_list[i][j] = pieces[count++];
            }
        }
        return pieces_list;
    }

    private String setInfoHash() {
        String sha1 = "";
        try {
            MessageDigest sha1digest = MessageDigest.getInstance("SHA1");
            sha1digest.update(info.getBencodeBytes());
            byte[] sha1_bytes = sha1digest.digest();
            for (int i = 0; i < sha1_bytes.length; i++) {
                sha1 += Integer.toString((sha1_bytes[i] & 0xff) + 0x100, 16).substring(1);
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(XhiftTorrent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sha1;
    }

    private byte[] setInfoHashBytes() {
        byte[] sha1_bytes = null;
        try {
            MessageDigest sha1digest = MessageDigest.getInstance("SHA1");
            sha1digest.update(info.getBencodeBytes());
            sha1_bytes = sha1digest.digest();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(XhiftTorrent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sha1_bytes;
    }
    
    public String getInfoHash() {
        return infoHash;
    }
    
    public byte[] getInfoHashBytes() {
        return infoHashBytes;
    }

    public URL getTracker() {
        return announce_url;
    }

    public int pieces() {
        return this.pieces.length;
    }

    public long getPieceLength() {
        return piece_length;
    }

    public byte[] getPieceHash(int piece_index) {
        return pieces[piece_index];
    }
}
