/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squgeim.xhifttorrent;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author squgeim
 */
public class XhiftClient {

    private Channel ch = null;
    private EventLoopGroup group = null;
    private Bootstrap b = null;

    XhiftClient(String host, int port, ByteBuf out) {
        group = new NioEventLoopGroup();
        try {
            b = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline p = socketChannel.pipeline();
                        p.addLast(new ClientHandler());
                    }
                });
            ch = b.connect(host, port).sync().channel();

//            ByteBuf out = ch.alloc().buffer(msg.length);
//            out.writeBytes(msg);
            System.out.println("Sending: " + out.toString(StandardCharsets.UTF_8));
            ch.writeAndFlush(out);

            ch.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
            group.shutdownGracefully();
        } finally {
            group.shutdownGracefully();
        }
    }

}
