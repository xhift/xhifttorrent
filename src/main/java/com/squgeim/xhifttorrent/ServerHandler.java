/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squgeim.xhifttorrent;

import com.squgeim.xhiftbencoder.BencodeDataType;
import com.squgeim.xhiftbencoder.BencodeDecoder;
import com.squgeim.xhiftbencoder.BencodeDictionary;
import com.squgeim.xhiftbencoder.BencodeString;
import com.squgeim.xhiftbencoder.throwables.IsNotBencode;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.CharsetUtil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author squgeim
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {

/*
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg;
        byte first_byte;
        if (in.isReadable()) {
            first_byte = in.readByte();
        } else {
            first_byte = 0;
        }
        final ByteBuf output = ctx.alloc().buffer();
        if (XhiftTracker.hasInstance() == true) {
            if (first_byte == 'G') {
                String get_request = 'G' + in.toString(io.netty.util.CharsetUtil.US_ASCII);
                Pattern p = Pattern.compile("^GET /(a|metainfo)\\?(.+) HTTP/1.1");
                Matcher m = p.matcher(get_request);
                if (m.find()) {
                    if (m.group(1).equals("a")) {
                        try {
                            String request = URLDecoder.decode(m.group(2), "UTF-8");
                            System.out.println(request);
                            BencodeDictionary metadict = (BencodeDictionary) BencodeDecoder.decodeBencode(request);

                            BencodeString info_hash = (BencodeString) metadict.get("info_hash");
                            if (info_hash == null) {
                                System.out.println("Not a proper announce request.");
                                ctx.close();
                                return;
                            }

                            if (metadict.get("ip") == null) {
                                metadict.put("ip", ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress());
                            }

                            XhiftTracker tr = XhiftTracker.getInstance();
                            tr.insertPeer(metadict);
                            BencodeDataType response = tr.getResponse(info_hash, (BencodeString) metadict.get("peer_id"));
                            output.writeBytes(response.getBencodeBytes());
                        } catch (IOException ex) {
                            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                            ((ByteBuf) msg).release();
                            ctx.close();
                        } catch (IsNotBencode ex) {
                            System.out.println("Invalid announce request.");
                            ((ByteBuf) msg).release();
                            ctx.close();
                        }
                    } else if (m.group(1).equals("metainfo")) {
                        String info_hash = m.group(2);
                        if (info_hash.equals(MetainfoDict.getInstance().getInfoHash())) {
                            byte[] metainfo_file = MetainfoDict.getInstance().getMetainfoBytes();
                            output.writeBytes(metainfo_file);
                        }
                    }
                } else {
                    System.out.println("Not GET request.");
                    //((ByteBuf) msg).release();
                    ctx.close();
                }
            }
        }
        output.writeBytes("this is some random message!".getBytes(StandardCharsets.UTF_8));
        (ctx.writeAndFlush(output)).addListener(ChannelFutureListener.CLOSE);
    }
*/

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String host = ((InetSocketAddress) ctx.channel().remoteAddress()).getHostString();
        int port = ((InetSocketAddress) ctx.channel().remoteAddress()).getPort();

        ByteBuf in = (ByteBuf) msg;

        byte first_byte;
        if (in.isReadable()) {
            first_byte = in.readByte();
            in.resetReaderIndex();
        } else {
            first_byte = 0;
        }
        final ByteBuf output = ctx.alloc().buffer();
        if (XhiftTracker.hasInstance()) {
            if (first_byte == 'G') {
                String get_request = in.toString(io.netty.util.CharsetUtil.US_ASCII);
                Pattern p = Pattern.compile("^GET /(a|metainfo)\\?(.+) HTTP/1.1");
                Matcher m = p.matcher(get_request);
                if (m.find()) {
                    if (m.group(1).equals("a")) {
                        try {
                            String request = URLDecoder.decode(m.group(2), "UTF-8");
                            System.out.println(request);
                            BencodeDictionary metadict = (BencodeDictionary) BencodeDecoder.decodeBencode(request);

                            BencodeString info_hash = (BencodeString) metadict.get("info_hash");
                            if (info_hash == null) {
                                System.out.println("Not a proper announce request.");
                                ctx.close();
                                return;
                            }

                            if (metadict.get("ip") == null) {
                                metadict.put("ip", host);
                            }

                            XhiftTracker tr = XhiftTracker.getInstance();
                            tr.insertPeer(metadict);
                            BencodeDataType response = tr.getResponse(info_hash, (BencodeString) metadict.get("peer_id"));
                            output.writeBytes(response.getBencodeBytes());
                            ctx.writeAndFlush(output);
                        } catch (IOException ex) {
                            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                            ctx.close();
                        } catch (IsNotBencode ex) {
                            System.out.println("Invalid announce request.");
                            ctx.close();
                        }
                    } else if (m.group(1).equals("metainfo")) {
                        String info_hash = m.group(2);
                        if (info_hash.equals(MetainfoDict.getInstance().getInfoHash())) {
                            byte[] metainfo_file = MetainfoDict.getInstance().getMetainfoBytes();
                            output.writeBytes(metainfo_file);
                        }
                    }
                } else {
                    System.out.println("Not GET request.");
                    //((ByteBuf) msg).release();
                    ctx.close();
                }
                return;
            }
        }


        Peers.Peer peer = Peers.getInstance().getPeerByIp(host);

        if(peer == null) {
            // this is not coming from any known peer.
            int length = in.readByte();

            byte[] protocol = "XhiftTorrent".getBytes(StandardCharsets.UTF_8);

            if (length != protocol.length) {
                System.out.println("length is not right! This is not a handshake message.");
                throw new InvalidRequest();
            }

            // ok this may be a handshake message.

            byte[] protocol_rcv = new byte[length];
            in.readBytes(protocol_rcv, 0, length);
            if (!protocol.equals(protocol_rcv)) {
                System.out.println("protocol is not right");
                throw new InvalidRequest();
            }

            byte[] reserved_bytes = new byte[8];
            in.readBytes(reserved_bytes, 0, 8);
            if (!reserved_bytes.equals(new byte[8])) {
                System.out.println("Has reserved bytes filled");
            }

            byte[] info_hash = new byte[20];
            in.readBytes(info_hash, 0, 20);

            byte[] peerId = new byte[20];
            in.readBytes(peerId, 0, 20);

            BencodeDictionary peer_info = new BencodeDictionary();
            peer_info.put("peer id", peerId);
            peer_info.put("ip", host);
            peer_info.put("port", port);

            Peers.getInstance().addPeer(peer_info);

            return;
        }

        int len = in.readInt();

        if (len == 0) {
            // keep alive message.
            peer.keepAlive();
            return;
        }

        byte id = in.readByte();
        byte[] message = new byte[len - 1];
        in.readBytes(message, 0, len - 1);
        switch (id) {
            case 0:
                // choke
                peer.isChoking();
                break;
            case 1:
                // unchoke
                peer.isUnchoking();
                break;
            case 2:
                // interested
                peer.isInterested();
                break;
            case 3:
                // not interested
                peer.isNotInterested();
                break;
            case 4:
                // have
                ByteBuffer piece_index = ByteBuffer.wrap(message, 0, 4);
                peer.havePiece(piece_index.getInt());
                break;
            case 5:
                // bitfield
                peer.loadBitfield(message);
                break;
            case 6:
                // request
                ByteBuffer req = ByteBuffer.wrap(message);
                int index = req.getInt();
                int begin = req.getInt();
                int piece_length = req.getInt();

                // send file piece block here!
                break;
            default:
                ctx.close();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

    public class InvalidRequest extends Exception {}
}
