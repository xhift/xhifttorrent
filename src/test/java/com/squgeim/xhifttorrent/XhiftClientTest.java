package com.squgeim.xhifttorrent;

import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.netty.channel.Channel;

public class XhiftClientTest {

	private XhiftClient client = null;
	@Before
	public void setUp() throws Exception {
	    client = new XhiftClient("localhost", 6969);
	}

	@After
	public void tearDown() throws Exception {
		client.closeClient();
	}

	@Test
	public void sendmessage() throws Exception {
		client.write("something else".getBytes(StandardCharsets.UTF_8));
	}

}
